# libpdfium-nojs

Open-source PDF rendering engine.

Required by megasync

https://pdfium.googlesource.com/pdfium/

How to clone this repo:

```
git clone https://gitlab.com/rebornos-team/rebornos-packages/cloud/libpdfium-nojs.git
```

If the following error occurs:

megasync: error while loading shared libraries: libicuuc.so.67: cannot open shared object file: No such file or directory

/usr/lib/libicuuc.so is owned by the `icu` package, since it has been updated, the package must be recompiled.

